package com.example.clovi.cookyapp.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.clovi.cookyapp.R;


/**
 * Created by clovi on 22/01/2017.
 */

public class Fr_plat extends Fragment {

    private View vue_fr_plat;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vue_fr_plat = inflater.inflate(R.layout.plat_fr, container, false);
        return vue_fr_plat;
    }
}
