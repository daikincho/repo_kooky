package com.example.clovi.cookyapp.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.clovi.cookyapp.R;

/**
 * Created by clovi on 18/01/2017.
 */

public class Fr_entree extends Fragment implements PagerAdapter {
    private View vue_fr_entree;
    private Button boutton_fr;
    //on ajoute les image dans un tabeau du swpe dans un tableau
    private int [] images_entree = (R.drawable.salade_entree, R.drawable.arc_en_ciel);

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return false;
    }

    public interface interface_boutton_fr{
        void affichier_toast();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vue_fr_entree = inflater.inflate(R.layout.entree_fr, container, false);
        return vue_fr_entree;
    }

}

