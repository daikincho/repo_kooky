package com.example.clovi.cookyapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.clovi.cookyapp.Fragment.Fr_entree;

/**
 * Created by clovi on 18/01/2017.
 */

public class deuxiemePage extends Activity implements Fr_entree.interface_boutton_fr {
    private FrameLayout container_Fr1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_deuxieme);
        container_Fr1 = (FrameLayout) findViewById(R.id.id_first_fr);
        addFragment();
    }

    public  void addFragment(){
        FragmentManager manager = getFragmentManager();
        FragmentTransaction tx = manager.beginTransaction();
        tx.replace(R.id.id_first_fr, new Fr_entree());
        tx.commit();
    }



    @Override
    public void affichier_toast() {
        Toast.makeText(this,"le boutton de frament a ete cliqué",Toast.LENGTH_SHORT).show();
    }
}
