package com.example.clovi.cookyapp.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.clovi.cookyapp.R;

/**
 * Created by clovi on 22/01/2017.
 */

public class Fr_dessert extends Fragment implements PagerAdapter{
    private View vue_fr_dessert;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vue_fr_dessert = inflater.inflate(R.layout.dessert_fr,container,false);
        return vue_fr_dessert;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return false;
    }
}
