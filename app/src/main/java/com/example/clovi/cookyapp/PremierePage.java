package com.example.clovi.cookyapp;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.clovi.cookyapp.Fragment.Fr_dessert;
import com.example.clovi.cookyapp.Fragment.Fr_entree;
import com.example.clovi.cookyapp.Fragment.Fr_plat;
import com.example.clovi.cookyapp.Fragment.Fr_tendence_droite;
import com.example.clovi.cookyapp.Fragment.Fr_tendence_gauche;

public class PremierePage extends AppCompatActivity {

    public FrameLayout container_entree;
    public FrameLayout container_plat;
    public FrameLayout container_dessert;
    public FrameLayout container_tendence_gauche;
    public FrameLayout Container_tendence_droite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premiere_page);
        container_entree = (FrameLayout) findViewById(R.id.id_fr_entrees);
        container_plat = (FrameLayout) findViewById(R.id.id_fr_plats);
        container_dessert = (FrameLayout) findViewById(R.id.id_fr_desserts);
      //  container_tendence_gauche =(FrameLayout) findViewById(R.id.id_fr_tendence_gauche);
     //   Container_tendence_droite = (FrameLayout) findViewById(R.id.id_fr_tendence_droite);
        addFragment_recettes();
    }

    public void addFragment_recettes(){
        FragmentManager manager = getFragmentManager();
        FragmentTransaction tx = manager.beginTransaction();
        tx.replace(R.id.id_fr_entrees,new Fr_entree());
        tx.replace(R.id.id_fr_plats, new Fr_plat());
        tx.replace(R.id.id_fr_desserts, new Fr_dessert());
    //    tx.replace(R.id.id_fr_tendence_gauche, new Fr_tendence_gauche());
    //    tx.replace(R.id.id_fr_tendence_droite, new Fr_tendence_droite());
        tx.commit();
    }



}
